<<<<<<< HEAD:Sender3.py
# /* Jingkai Yuan 1941321 */
=======
#/* Jingkai Yuan 1941321 */
>>>>>>> 812ed695d88b9008f7346db3a03c4d3a527726d0:send3.py
from socket import *
import math
from threading import Thread, Lock
import sys
import time

# receiving parameters
serverName = sys.argv[1]
serverPort = int(sys.argv[2])
with open(sys.argv[3], 'rb') as f:
    file = f.read()
    bytes_data = bytearray(file)
    f.close()
retryTimeout = float(int(sys.argv[4]) / 1000)
windowSize = int(sys.argv[5])
# construct the server address
server_address = (serverName, serverPort)
# create the client socket
clientSocket = socket(AF_INET, SOCK_DGRAM)
# initiate parameters
lastSeqNum = math.ceil(float(len(bytes_data)) / float(1024)) - 1
# next sequence number needs to be sent
nextSeqNum = 0
# current sending window base
base = 0
# retransmission times counter
retransmissions = 0
# if all package has been sent and all ack have been received, it turns True, otherwise False
isDelivered = False
# is timer started
timerStarted = False
startTime = float(-1)
threads = []
lock = Lock()


def sending():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global windowSize
    global startTime
    global lastSeqNum
    while True:
        if isDelivered:
            break
        lock.acquire()
        if nextSeqNum - base <= windowSize + 1 and nextSeqNum <= lastSeqNum:
            if nextSeqNum == lastSeqNum:
                EOF = 1
            else:
                EOF = 0
            packet = bytearray(nextSeqNum.to_bytes(2, byteorder='big'))
            packet.append(EOF)
            if EOF == 0:
                packet.extend(bytes_data[nextSeqNum * 1024:nextSeqNum * 1024 + 1024])
            if EOF == 1:
                packet.extend(bytes_data[nextSeqNum * 1024:])
            clientSocket.sendto(packet, server_address)
            print("send: ", nextSeqNum)
            if base == nextSeqNum:
                timerStarted = True
                startTime = time.time()
            nextSeqNum += 1
        lock.release()


def receiveACK():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global startTime
    while True:
        if isDelivered:
            break
        data, addr = clientSocket.recvfrom(2)
        ackSeqNum = int.from_bytes(data[:2], 'big')
        lock.acquire()
        print("receive: ", ackSeqNum)
        if base <= ackSeqNum:
            base = ackSeqNum
            startTime = time.time()
            timerStarted = True
            if base == lastSeqNum:
                isDelivered = True
        if base == nextSeqNum:
            timerStarted = False
        lock.release()


def timeout():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global startTime
    global retryTimeout
    while True:
        if isDelivered:
            break
        lock.acquire()
        currentTime = time.time()
        if currentTime - startTime > retryTimeout and timerStarted:
            print("timeout!!! with seqNum: ", nextSeqNum)
            retransmissions += 1
            startTime = time.time()
            timerStarted = True
            nextSeqNum = base
            if base == lastSeqNum:
                isDelivered = True
                break
        lock.release()


# sending start
start = time.perf_counter()
# start receiveACK threading
receiveACKThread = Thread(target=receiveACK)
receiveACKThread.start()
threads.append(receiveACKThread)
# start timeout threading
timeoutThread = Thread(target=timeout)
timeoutThread.start()
threads.append(timeoutThread)
# start sending threading
sender = Thread(target=sending)
sender.start()
threads.append(sender)
# join the threading together
for t in threads:
    t.join()
# close socket
clientSocket.close()
# sending ends
end = time.perf_counter()
totalTimeUsed = end - start

print("total time cost: " + str(totalTimeUsed) + '\n')
print("retransmission times: " + str(retransmissions) + '\n')
print("throughput: " + str(len(bytes_data) / totalTimeUsed / 1000) + '\n')
