#/* Jingkai Yuan 1941321 */
from socket import *
import sys
import time

serverIP = 'localhost'
serverPort = int(sys.argv[1])
fileName = sys.argv[2]
windowSize = int(sys.argv[3])

serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind((serverIP,serverPort))
print('Server is running')
data = bytearray()

cacheDict = dict()

LastPackage = False
fileReceived = False
receiverBase = 0
isLastOne = False
lastSeqNum = 1000000
address = 0

while not fileReceived:
    packet, addr = serverSocket.recvfrom(1027)
    address = addr
    seqNum = int.from_bytes(packet[:2], 'big')
    #print("seqNum",seqNum)
    if packet[2] == 1:
        lastSeqNum = seqNum
    if seqNum in range(receiverBase - windowSize, receiverBase):
        pktBack = bytearray(seqNum.to_bytes(2, byteorder='big'))
        serverSocket.sendto(pktBack, addr)

    if receiverBase <= seqNum < receiverBase + windowSize:
        pktBack = bytearray(seqNum.to_bytes(2, byteorder='big'))
        serverSocket.sendto(pktBack, addr)
        cacheDict[seqNum] = packet[3:]

        if seqNum == receiverBase:
            isAllReceived = True
            for packetNum in range(receiverBase,receiverBase+windowSize):
                if packetNum not in cacheDict.keys():
                    receiverBase = packetNum
                    isAllReceived = False
                    break
                else:
                    data.extend(cacheDict[packetNum])
            if isAllReceived:
                receiverBase = receiverBase + windowSize
            if receiverBase-1 == lastSeqNum:
                fileReceived = True

        #print("receiveBase",receiverBase)
        #print("lastSeqNum",lastSeqNum)
    #print("is file received? ", fileReceived)
    print("len data", len(data))
for i in range(10):
    print("send last acks within the window")
    for packets in range(lastSeqNum - windowSize, lastSeqNum + 1):
        pktBack = bytearray(packets.to_bytes(2, byteorder='big'))
        serverSocket.sendto(pktBack, address)
with open(fileName, 'wb') as f:
    f.write(data)
    f.close()
# for i in range(5):
#     print(i)
#     serverSocket.sendto(bytearray((lastSeqNum+10).to_bytes(2, byteorder='big')), address)
serverSocket.close()
