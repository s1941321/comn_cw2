<<<<<<< HEAD
# /* Jingkai Yuan 1941321 */
=======
#/* Jingkai Yuan 1941321 */
>>>>>>> 812ed695d88b9008f7346db3a03c4d3a527726d0
from threading import Thread, Lock
import time
import sys
from socket import *
import math

serverName = sys.argv[1]
serverPort = int(sys.argv[2])
fileName = open(sys.argv[3], 'rb')
retryTimeout = int(sys.argv[4]) / 1000
windowSize = int(sys.argv[5])

# construct the server address
server_address = (serverName, serverPort)

# create the client socket
clientSocket = socket(AF_INET, SOCK_DGRAM)

# read the file(image) and save in a byte array
file = fileName.read()
bytes_data = bytearray(file)

# size of the data
numberOfPackets = len(bytes_data)
print(numberOfPackets)

# initiate parameters
lastSeqNum = math.ceil((len(bytes_data) / 1024) - 1)
nextSeqNum = 0
base = 0
retransmissions = 0
lastWindowResend = 0
isDelivered = False
TimerStart = False
timerStarted = False
isLastWindow = False
startTime = time.time()

lock = Lock()
receivedACK = set()

# target function for sending the
def sending():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global windowSize
    global startTime
    while True:
        if isDelivered:
            break
        lock.acquire()
        if nextSeqNum - base < windowSize and nextSeqNum <= lastSeqNum:
            if nextSeqNum == lastSeqNum:
                EOF = 1
            else:
                EOF = 0
            packet = bytearray(nextSeqNum.to_bytes(2, byteorder='big'))
            packet.append(EOF)
            if EOF == 0:
                packet.extend(bytes_data[nextSeqNum * 1024:nextSeqNum * 1024 + 1024])
            if EOF == 1:
                packet.extend(bytes_data[nextSeqNum * 1024:])
            print("send", nextSeqNum)
            clientSocket.sendto(packet, server_address)
            if base == nextSeqNum:
                timerStarted = True
                startTime = time.perf_counter()
            nextSeqNum += 1
        lock.release()


def receiveACK():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global startTime
    global isLastWindow
    while True:
        if isDelivered:
            break
        # receive ack from receiver
        data, addr = clientSocket.recvfrom(2)
        ackSeqNum = int.from_bytes(data[:2], 'big')
        # lock-in
        lock.acquire()
        # add the received ACK into a set
        receivedACK.add(ackSeqNum)
        print("ackSize", len(receivedACK))
        if ackSeqNum == base:
            isAllReceived = True
            for ack in range(base, nextSeqNum):
                if ack not in receivedACK:
                    base = ack
                    isAllReceived = False
                    break
            if isAllReceived:
                base = nextSeqNum
                timerStarted = False
            else:
                timerStarted = True
                startTime = time.perf_counter()
        if len(receivedACK) == lastSeqNum + 1:
            isDelivered = True
        lock.release()


def timeout():
    global nextSeqNum
    global base
    global timerStarted
    global isDelivered
    global retransmissions
    global startTime
    global lastWindowResend
    global isLastWindow
    while True:
        if isDelivered:
            break
        lock.acquire()
        # if lastWindowResend == 20:
        #     print("warning!")
        #     isDelivered = True
        if len(receivedACK) == lastSeqNum + 1:
            isDelivered = True
        currentTime = time.perf_counter()
        if currentTime - startTime > retryTimeout and timerStarted:
            for ack in range(base, nextSeqNum):
                if ack not in receivedACK:
                    if ack == lastSeqNum:
                        EOF = 1
                    else:
                        EOF = 0
                    packet = bytearray(ack.to_bytes(2, byteorder='big'))
                    packet.append(EOF)
                    if EOF == 0:
                        packet.extend(bytes_data[ack * 1024:ack * 1024 + 1024])
                    if EOF == 1:
                        packet.extend(bytes_data[ack * 1024:])
                    print("resend", ack)
                    print("base", base)
                    clientSocket.sendto(packet, server_address)
                    retransmissions += 1
                    timerStarted = True
                    startTime = time.perf_counter()
        # if base in range(lastSeqNum-windowSize,lastSeqNum+1):
        #     data, addr = clientSocket.recvfrom(2)
        #     check = int.from_bytes(data[:2], 'big')
        #     if check == lastSeqNum + 10:
        #         isDelivered = True
        #         print("bingo")
        lock.release()


# def LastWindow():
#     global nextSeqNum
#     global base
#     global timerStarted
#     global isDelivered
#     global retransmissions
#     global startTime
#     global lastWindowResend
#     global isLastWindow
#     while True:
#         if isDelivered:
#             break
#         lock.acquire()
#
#         lock.release()

threads = []

# sending start
start = time.perf_counter()

# start threads
receiveACKThread = Thread(target=receiveACK)
receiveACKThread.start()
threads.append(receiveACKThread)
timeoutThread = Thread(target=timeout)
timeoutThread.start()
threads.append(timeoutThread)
# start sending
sender = Thread(target=sending)
sender.start()
threads.append(sender)
for t in threads:
    t.join()
# sending ends
end = time.perf_counter()

# print info
totalTimeUsed = end - start
print("retryTimeout: " + str(retryTimeout))
print("total time cost: " + str(totalTimeUsed) + '\n')
print("retransmission times: " + str(retransmissions) + '\n')
print("throughput: " + str(len(bytes_data) / totalTimeUsed / 1000))
