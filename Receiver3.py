#/* Jingkai Yuan 1941321 */
from socket import *
import sys

# script parameters
serverIP = 'localhost'
serverPort = int(sys.argv[1])
fileName = sys.argv[2]

# create server socket and bind it
serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind((serverIP,serverPort))
print('The server is ready to receive')
data = bytearray()

# initiate parameters
seqNum = 0
expectedSeqNum = 0
fileReceived = False

while fileReceived == False:
    # receive data within size of 1027 bytes
    packet, addr = serverSocket.recvfrom(1027)
    seqNum = int.from_bytes(packet[:2], 'big')
    print("expected seqNum: ", expectedSeqNum)
    print("receive: ",seqNum)
    if seqNum == expectedSeqNum:
        data.extend(packet[3:])
        expectedSeqNum += 1
    if expectedSeqNum == 0:
        var = 0
    else:
        var = expectedSeqNum - 1
    pktBack = bytearray(var.to_bytes(2, byteorder='big'))
    serverSocket.sendto(pktBack,addr)
    print("send ack:",var)
    # when the seqNum is not the one we expected, keep receiving the expected seqNum until we gets it right
    while expectedSeqNum-1 != seqNum:
        # keep receiving data from the
        packet, addr = serverSocket.recvfrom(1027)
        seqNum = int.from_bytes(packet[:2], 'big')
        print("expected seqNum: ", expectedSeqNum)
        print("receive: ", seqNum)
        if seqNum == expectedSeqNum:
            data.extend(packet[3:])
            expectedSeqNum += 1
        if expectedSeqNum == 0:
            var = 0
        else:
            var = expectedSeqNum - 1
        pktBack = bytearray(var.to_bytes(2, byteorder='big'))
        serverSocket.sendto(pktBack, addr)
    if packet[2] == 1:
        pktBack = bytearray(seqNum.to_bytes(2, byteorder='big'))
        for i in range(5):
            serverSocket.sendto(pktBack, addr)
        fileReceived = True
with open(fileName, 'wb') as f:
    f.write(data)
    f.close()
serverSocket.close()